struct AABB {
    vec2i min;
    vec2i max;

    AABB() {}
    AABB(vec2i _min, vec2i _max) {min = _min; max = _max;}

    int half_width() const {return (max.x - min.x) / 2;}
    int half_height() const {return (max.y - min.y) / 2;}
    vec2i left_center() const {return vec2i(min.x, min.y + half_height());}
    vec2i right_center() const {return vec2i(max.x, min.y + half_height());}
    vec2i bottom_center() const {return vec2i(min.x + half_width(), min.y);}
    vec2i top_center() const {return vec2i(min.x + half_width(), max.y);}
    vec2i center() const {return vec2i(min.x + half_width(), min.y + half_height());}
    AABB sw_subregion() const {return AABB(min, center());}
    AABB nw_subregion() const {return AABB(left_center(), top_center());}
    AABB ne_subregion() const {return AABB(center(), max);}
    AABB se_subregion() const {return AABB(bottom_center(), right_center());}
};

bool IntersectionTest(const AABB& first, const AABB& second) {
    if(first.max.x < second.min.x || first.min.x > second.max.x) return false;
    if(first.max.y < second.min.y || first.min.y > second.max.y) return false;
    return 1;
}

bool IsPointInAABB(const vec2i point, const AABB& box) {
    if(point.x >= box.min.x && point.x <= box.max.x &&
       point.y >= box.min.y && point.y <= box.max.y) {
        return true;
    } else return false;
}

template<typename T>
class Array 
{
public:
    int                         Size;
    int                         Capacity;
    T*                          Data;

    typedef T                   value_type;
    typedef value_type*         iterator;
    typedef const value_type*   const_iterator;

    Array()                  { Size = Capacity = 0; Data = NULL; }
    ~Array()                 { if (Data) free(Data); }

    inline bool                 empty() const                   { return Size == 0; }
    inline int                  size() const                    { return Size; }
    inline int                  capacity() const                { return Capacity; }

    inline value_type&          operator[](int i)               { AJ_ASSERT(i < Size); return Data[i]; }
    inline const value_type&    operator[](int i) const         { AJ_ASSERT(i < Size); return Data[i]; }

    inline void                 clear()                         { if (Data) { Size = Capacity = 0; free(Data); Data = NULL; } }
    inline iterator             begin()                         { return Data; }
    inline const_iterator       begin() const                   { return Data; }
    inline iterator             end()                           { return Data + Size; }
    inline const_iterator       end() const                     { return Data + Size; }
    inline value_type&          front()                         { AJ_ASSERT(Size > 0); return Data[0]; }
    inline const value_type&    front() const                   { AJ_ASSERT(Size > 0); return Data[0]; }
    inline value_type&          back()                          { AJ_ASSERT(Size > 0); return Data[Size-1]; }
    inline const value_type&    back() const                    { AJ_ASSERT(Size > 0); return Data[Size-1]; }
    inline void                 swap(Array<T>& rhs)          { int rhs_size = rhs.Size; rhs.Size = Size; Size = rhs_size; int rhs_cap = rhs.Capacity; rhs.Capacity = Capacity; Capacity = rhs_cap; value_type* rhs_data = rhs.Data; rhs.Data = Data; Data = rhs_data; }

    inline int                  _grow_capacity(int new_size)    { int new_capacity = Capacity ? (Capacity + Capacity/2) : 8; return new_capacity > new_size ? new_capacity : new_size; }

    inline void                 resize(int new_size)            { if (new_size > Capacity) reserve(_grow_capacity(new_size)); Size = new_size; }
    inline void                 reserve(int new_capacity)
    {
        if (new_capacity <= Capacity) return;
        T* new_data = (value_type*)malloc((size_t)new_capacity * sizeof(value_type));
        if (Data)
            memcpy(new_data, Data, (size_t)Size * sizeof(value_type));
        free(Data);
        Data = new_data;
        Capacity = new_capacity;
    }

    inline void                 push_back(const value_type& v)  { if (Size == Capacity) reserve(_grow_capacity(Size+1)); Data[Size++] = v; }
    inline void                 pop_back()                      { AJ_ASSERT(Size > 0); Size--; }

    inline iterator             erase(const_iterator it)        { AJ_ASSERT(it >= Data && it < Data+Size); const ptrdiff_t off = it - Data; memmove(Data + off, Data + off + 1, ((size_t)Size - (size_t)off - 1) * sizeof(value_type)); Size--; return Data + off; }
    inline iterator             insert(const_iterator it, const value_type& v)  { AJ_ASSERT(it >= Data && it <= Data+Size); const ptrdiff_t off = it - Data; if (Size == Capacity) reserve(Capacity ? Capacity * 2 : 4); if (off < (int)Size) memmove(Data + off + 1, Data + off, ((size_t)Size - (size_t)off) * sizeof(value_type)); Data[off] = v; Size++; return Data + off; }

    inline int                  search(const value_type& v)
    {
        for(int i = 0; i < size(); ++i) {
            if(Data[i] == v) return i;
        }
        return -1;
    }
};
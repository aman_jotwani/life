#include <immintrin.h>

#ifdef _WIN32
#define Align(Alignment) __declspec(align((Alignment)))
#elif (__APPLE__ && __MACH__)
#define Align(Alignment) __attribute__((aligned((Alignment))))
#endif

#define SIMD_ALIGN Align(16)

static double PI = 3.1415;
#define ToRadians(x) (x)*(PI/180.0)

struct vec2i {
    int32_t x, y;

    vec2i():x(0), y(0) {};
    vec2i(int32_t _x, int32_t _y):x(_x), y(_y) {};
    void set(int32_t _x, int32_t _y) {x = _x; y = _y;}
    void set(const vec2i& other) {x = other.x; y = other.y;}

    bool operator==(const vec2i& other) {
        return ((x == other.x) && (y == other.y) ? true : false);
    }
    vec2i operator/(int scalar) {
        return vec2i(x / scalar, y / scalar);
    }

    vec2i operator+(const vec2i& other) {
        return vec2i(x + other.x, y + other.y);
    }
    
    vec2i operator-(const vec2i& other) {
        return vec2i(x - other.x, y - other.y);
    }
};

vec2i operator*(const vec2i& first, int scalar) {
    return vec2i(first.x*scalar, first.y*scalar);
}

vec2i operator*(int scalar, const vec2i& second) {
    return vec2i(scalar*second.x, scalar*second.y);
}

bool operator!=(const vec2i& first, const vec2i& other) {
    return ((first.x != other.x) || (first.y != other.y) ? true : false);
}

struct vec2 {
    float x, y;

    vec2():x(0), y(0) {};
    vec2(float _x, float _y):x(_x), y(_y) {};
    vec2(vec2i v) {x = (float)v.x; y = (float)v.y;}
    void set(float _x, float _y) {x = _x; y = _y;};

    vec2 operator*(int scalar) {
        vec2 result(x*scalar, y*scalar);
        return result;
    }

    bool operator==(const vec2& other) {
        return ((x == other.x) && (y == other.y) ? true : false);
    }

    bool operator!=(const vec2& other) {
        return ((x != other.x) || (y != other.y) ? true : false);
    }
};

struct matrix
{
    SIMD_ALIGN float column_major[16];
    matrix();
};

matrix::matrix()
{
    for(int i = 0; i < 16; ++i)
    {
        column_major[i] = 0.0f;
    }
}

matrix IdentityMatrix()
{
    matrix Result;
    Result.column_major[0] = 1.0f;
    Result.column_major[5] = 1.0f;
    Result.column_major[10] = 1.0f;
    Result.column_major[15] = 1.0f;
    return(Result);
}

matrix Transpose(const matrix& other)
{
    matrix result;
    int row_num = 0;
    int k = row_num;
    for(int i = 0; i < 16; i++)
    {
        result.column_major[i] = other.column_major[k];
        k += 4;
        if(k >= 16)
        {
            k = ++row_num;
        }
    }
    return(result);
}

matrix StripTranslation(matrix other)
{
    matrix result = other;
    result.column_major[12] = 0.0f;
    result.column_major[13] = 0.0f;
    result.column_major[14] = 0.0f;
    return(result);
}

matrix Translation(float x, float y, float z)
{
    matrix result;
    result.column_major[0] = 1.0f;
    result.column_major[5] = 1.0f;
    result.column_major[10] = 1.0f;
    result.column_major[12] = x;
    result.column_major[13] = y;
    result.column_major[14] = z;
    result.column_major[15] = 1.0f;
    return(result);
}

matrix Scaling(float x, float y, float z)
{
    matrix result;
    result.column_major[0] = x;
    result.column_major[5] = y;
    result.column_major[10] = z;
    result.column_major[15] = 1.0f;
    return(result);
}

matrix RotationX(float AngleInDegrees)
{
    double AngleInRadians = ToRadians(AngleInDegrees);
    matrix result;
    result.column_major[0] = 1.0f;
    result.column_major[5] = cos(AngleInRadians);
    result.column_major[6] = sin(AngleInRadians);
    result.column_major[9] = -sin(AngleInRadians);
    result.column_major[10] = cos(AngleInRadians);
    result.column_major[15] = 1.0f;
    return(result);
}

matrix RotationY(float AngleInDegrees)
{
    double AngleInRadians = ToRadians(AngleInDegrees);
    matrix result;
    result.column_major[0] = cos(AngleInRadians);
    result.column_major[2] = sin(AngleInRadians);
    result.column_major[5] = 1.0f;
    result.column_major[8] = -sin(AngleInRadians);
    result.column_major[10] = cos(AngleInRadians);
    result.column_major[15] = 1.0f;
    return(result); 
}

matrix RotationZ(float AngleInDegrees)
{
    double AngleInRadians = ToRadians(AngleInDegrees);
    matrix result;
    result.column_major[0] = cos(AngleInRadians);
    result.column_major[1] = sin(AngleInRadians);
    result.column_major[4] = -sin(AngleInRadians);
    result.column_major[5] = cos(AngleInRadians);
    result.column_major[10] = 1.0f;
    result.column_major[15] = 1.0f;
    return(result);     
}

// maps x:[0, Width], y:[0, Height] and z:[Near, Far] to OpenGL ndc unit cube
matrix OrthoProjectionOffCenterLH(float Near, float Far, float Width, float Height)
{
    matrix result;
    result.column_major[0] = 2/Width;
    result.column_major[5] = 2/Height;
    result.column_major[10] = 2/(Far-Near);
    result.column_major[12] = -1.0f;
    result.column_major[13] = -1.0f;
    result.column_major[14] = -(Far+Near)/(Far-Near);
    result.column_major[15] = 1.0f;
    return(result);
}

// maps x:[-Width/2, Width/2] y:[-Height/2, Height/2] and z:[Near, Far] to OpenGL ndc unit cube
matrix OrthoProjectionLH(float Near, float Far, float Width, float Height)
{
    matrix result;
    result.column_major[0] = 2/Width;
    result.column_major[5] = 2/Height;
    result.column_major[10] = 2/(Far-Near);
    result.column_major[14] = -(Far+Near)/(Far-Near);
    result.column_major[15] = 1.0f;
    return(result); 
}

// maps x:[-Width/2, Width/2] y:[-Height/2, Height/2] and z:[Near, Far] to OpenGL ndc unit cube
matrix OrthoProjectionRH(float Near, float Far, float Width, float Height)
{
    matrix result;
    result.column_major[0] = 2/Width;
    result.column_major[5] = 2/Height;
    result.column_major[10] = -2/(Far-Near);
    result.column_major[14] = -(Far+Near)/(Far-Near);
    result.column_major[15] = 1.0f;
    return(result); 
}

matrix PerspProjectionLH(float Near, float Far, float Fovy, float AspectRatio)
{
    matrix result;
    float tan_part = tan(ToRadians(Fovy/2));
    result.column_major[0] = 1.0f / (tan_part*AspectRatio);
    result.column_major[5] = 1.0f / tan_part;
    result.column_major[10] = (Far + Near) / (Far - Near);
    result.column_major[11] = 1.0f;
    result.column_major[14] = -2.0f*Near*Far / (Far - Near);
    return(result);
}

matrix PerspProjectionRH(float Near, float Far, float Fovy, float AspectRatio)
{
    matrix result;
    float tan_part = tan(ToRadians(Fovy/2));
    result.column_major[0] = 1.0f / (tan_part*AspectRatio);
    result.column_major[5] = 1.0f / tan_part;
    result.column_major[10] = -(Far + Near) / (Far - Near);
    result.column_major[11] = -1.0f;
    result.column_major[14] = -2.0f*Near*Far / (Far - Near);
    return(result);
}

float DotProduct(__m128 a, __m128 b)
{
    static SIMD_ALIGN float answer[4];
    const uint8_t control = 0x01;
    __m128 mulled = _mm_mul_ps(a, b);
    
    __m128 tmp0 = _mm_movehl_ps(mulled, mulled);
    __m128 tmp1 = _mm_add_ps(mulled, tmp0);
    __m128 tmp2 = _mm_shuffle_ps(tmp1, tmp1, control);
    __m128 result = _mm_add_ps(tmp1, tmp2);

    _mm_store_ps1((float*)answer, result);
    return(answer[0]);
}

matrix Mul(const matrix& first, const matrix& second)
{
    matrix result;
    __m128 frows[4];
    __m128 scols[4];

    frows[0] = _mm_load_ps((float*)first.column_major);
    frows[1] = _mm_load_ps((float*)(first.column_major + 4));
    frows[2] = _mm_load_ps((float*)(first.column_major+8));
    frows[3] = _mm_load_ps((float*)(first.column_major+12));
    _MM_TRANSPOSE4_PS(frows[0], frows[1], frows[2], frows[3]);

    scols[0] = _mm_load_ps((float*)second.column_major);
    scols[1] = _mm_load_ps((float*)(second.column_major+4));
    scols[2] = _mm_load_ps((float*)(second.column_major+8));
    scols[3] = _mm_load_ps((float*)(second.column_major+12));
    
    for(int i = 0; i < 16; ++i)
    {
        result.column_major[i] = DotProduct(frows[i/4], scols[i%4]);
    }
    result = Transpose(result);
    return(result);
}

__m128 MatVecMul(const matrix& first, const __m128 vec)
{
    __m128 result;
    __m128 frows[4];
    frows[0] = _mm_load_ps((float*)first.column_major);
    frows[1] = _mm_load_ps((float*)(first.column_major + 4));
    frows[2] = _mm_load_ps((float*)(first.column_major+8));
    frows[3] = _mm_load_ps((float*)(first.column_major+12));
    _MM_TRANSPOSE4_PS(frows[0], frows[1], frows[2], frows[3]);

    SIMD_ALIGN float vals[4];
    for(int i = 0; i < 4; ++i)
    {
        vals[i] = DotProduct(frows[i], vec);
    }
    result = _mm_load_ps((float*)vals);
    return(result);
}
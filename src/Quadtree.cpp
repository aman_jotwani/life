#define AJ_DUMMYPOINT vec2i(-1, -1)

// improvements:
// 1) freeing children when they don't hold data
// 2) handling the Status of Quadtree_Node
struct Quadtree_Node {
    AABB region;
    vec2i point;
    enum Status : uint8_t {
        Default = 0,
        Alive = 1,
        ToBeRemoved = 2,
        ToBeAdded = 4
    } flags;
    union {
        Quadtree_Node* children[4];
        struct {
            Quadtree_Node* sw;
            Quadtree_Node* nw;
            Quadtree_Node* ne;
            Quadtree_Node* se;
        };
    };

    Quadtree_Node() {
        flags = Default;
        point = AJ_DUMMYPOINT;
        for(int i = 0; i < 4; ++i) {
            children[i] = NULL;
        }
    }
    Quadtree_Node(AABB _region) {
        flags = Default;
        region = _region;
        point = AJ_DUMMYPOINT;
        for(int i = 0; i < 4; ++i) {
            children[i] = NULL;
        }
    }

    bool has_no_children() const {
        return !(children[0]);
    }
};

bool is_live_cell(Quadtree_Node::Status flag) {
    return (flag == Quadtree_Node::Alive || flag == Quadtree_Node::ToBeRemoved) ? true:false;
}

bool is_dead_cell(Quadtree_Node::Status flag) {
    return (flag == Quadtree_Node::Default || flag == Quadtree_Node::ToBeAdded) ? true:false;
}

vec2i GetCellBottomLeft(vec2i pointInCell, int cellSize) {
    vec2i bottomLeft = (pointInCell / cellSize) * cellSize;
    return bottomLeft;
}
vec2i GetCellCenter(vec2i pointInCell, int cellSize) {
    vec2i bottomLeft = GetCellBottomLeft(pointInCell, cellSize);
    vec2i center(bottomLeft.x + cellSize/2, bottomLeft.y + cellSize/2);
    return center;
}

void QuadtreeInsert(Quadtree_Node* curr_node, vec2i new_point, int cellSize, Quadtree_Node::Status initFlag = Quadtree_Node::Alive) {
    if(IsPointInAABB(new_point, curr_node->region)) {
        if(curr_node->has_no_children()) { // if node does not have any children
            if(curr_node->flags == Quadtree_Node::Default) { // if it's empty, just set
                curr_node->point = new_point;
                curr_node->flags = initFlag;
                return;
            } else if(curr_node->point == new_point) {
                curr_node->point = AJ_DUMMYPOINT;
                curr_node->flags = Quadtree_Node::Default;
                return;
            } else { // if it's not empty and has no children, check if it can be subdivided
                if(curr_node->region.half_width() >= cellSize && 
                curr_node->region.half_height() >= cellSize) { // check if the region is divisible
                    // create new children
                    curr_node->sw = new Quadtree_Node(curr_node->region.sw_subregion());
                    curr_node->nw = new Quadtree_Node(curr_node->region.nw_subregion());
                    curr_node->ne = new Quadtree_Node(curr_node->region.ne_subregion());
                    curr_node->se = new Quadtree_Node(curr_node->region.se_subregion());

                    // propagate the parent's data
                    QuadtreeInsert(curr_node->sw, curr_node->point, cellSize, curr_node->flags);
                    QuadtreeInsert(curr_node->nw, curr_node->point, cellSize, curr_node->flags);
                    QuadtreeInsert(curr_node->ne, curr_node->point, cellSize, curr_node->flags);
                    QuadtreeInsert(curr_node->se, curr_node->point, cellSize, curr_node->flags);

                    curr_node->point = AJ_DUMMYPOINT;
                } else {
                    printf("Something happened while inserting %d, %d.\n", new_point.x, new_point.y);
                    return;
                }
            }
        }
        // we have created children if they didn't exist, at this point
        // find the child node
        QuadtreeInsert(curr_node->sw, new_point, cellSize, initFlag);
        QuadtreeInsert(curr_node->nw, new_point, cellSize, initFlag);
        QuadtreeInsert(curr_node->ne, new_point, cellSize, initFlag);
        QuadtreeInsert(curr_node->se, new_point, cellSize, initFlag);
    }
}

void QuadtreeRangeQuery(int* liveNbors, const Quadtree_Node& currNode, AABB range) {
    if(currNode.has_no_children()) {
        if(is_live_cell(currNode.flags) && IsPointInAABB(currNode.point, range)) {
            AJ_ASSERT(currNode.point != AJ_DUMMYPOINT);
            (*liveNbors) += 1;
        }
    } else {
        bool swTest = IntersectionTest(range, currNode.sw->region);
        bool nwTest = IntersectionTest(range, currNode.nw->region);
        bool neTest = IntersectionTest(range, currNode.ne->region);
        bool seTest = IntersectionTest(range, currNode.se->region);

        if(swTest) QuadtreeRangeQuery(liveNbors, *currNode.sw, range);
        if(nwTest) QuadtreeRangeQuery(liveNbors, *currNode.nw, range);
        if(neTest) QuadtreeRangeQuery(liveNbors, *currNode.ne, range);
        if(seTest) QuadtreeRangeQuery(liveNbors, *currNode.se, range);
    }
}

Quadtree_Node::Status  QuadtreePointQuery(const Quadtree_Node& currNode, vec2i queryPoint) {
    if(currNode.has_no_children()) {
        return queryPoint == currNode.point ? currNode.flags:Quadtree_Node::Default;
    } else {
        bool swTest = IsPointInAABB(queryPoint, currNode.sw->region);
        bool nwTest = IsPointInAABB(queryPoint, currNode.nw->region);
        bool neTest = IsPointInAABB(queryPoint, currNode.ne->region);
        bool seTest = IsPointInAABB(queryPoint, currNode.se->region);

        if(swTest) return QuadtreePointQuery(*currNode.sw, queryPoint);
        if(nwTest) return QuadtreePointQuery(*currNode.nw, queryPoint);
        if(neTest) return QuadtreePointQuery(*currNode.ne, queryPoint);
        if(seTest) return QuadtreePointQuery(*currNode.se, queryPoint);
        else return Quadtree_Node::Default;
    }
}

struct Dead_Cell {
    vec2i coords;
    int32_t counter;

    Dead_Cell() {coords = AJ_DUMMYPOINT; counter = 1;}
    Dead_Cell(vec2i _coords) {coords = _coords; counter = 1;}
    bool operator==(const Dead_Cell& other) {
        return (coords == other.coords) ? true:false;
    }
};

void add_dead_cell(Array<Dead_Cell>& dead_cells, const Quadtree_Node& root_node, vec2i query_point) {
    if(is_dead_cell(QuadtreePointQuery(root_node, query_point))) {
        int index = dead_cells.search(Dead_Cell(query_point));
        if(index < 0) dead_cells.push_back(query_point);
        else dead_cells[index].counter += 1;
    }
}

void QuadtreeSimulateLife(const Quadtree_Node& rootNode, Quadtree_Node* currNode, Array<Dead_Cell>& dead_cells, int cellSize) {
    if(currNode->has_no_children()) {
        if(currNode->flags == Quadtree_Node::Alive) {
            int live_nbors = 0;
            QuadtreeRangeQuery(&live_nbors, rootNode, AABB(vec2i(currNode->point.x - cellSize,
                                                                 currNode->point.y - cellSize),
                                                            vec2i(currNode->point.x + cellSize,
                                                                  currNode->point.y + cellSize)));
            live_nbors -= 1; // not counting the cell itself
            if(live_nbors < 2 || live_nbors > 3) {
                currNode->flags = Quadtree_Node::ToBeRemoved;
            }

            int topY = currNode->point.y + cellSize;
            int bottomY = currNode->point.y - cellSize;
            int rightX = currNode->point.x + cellSize;
            int leftX = currNode->point.x - cellSize;

            add_dead_cell(dead_cells, rootNode, vec2i(leftX, topY)); // topleft
            add_dead_cell(dead_cells, rootNode, vec2i(rightX, topY)); // topright
            add_dead_cell(dead_cells, rootNode, vec2i(currNode->point.x, topY)); // topcenter
            add_dead_cell(dead_cells, rootNode, vec2i(leftX, bottomY)); // bottomleft
            add_dead_cell(dead_cells, rootNode, vec2i(rightX, bottomY)); // bottomright
            add_dead_cell(dead_cells, rootNode, vec2i(currNode->point.x, bottomY)); // bottomcenter
            add_dead_cell(dead_cells, rootNode, vec2i(leftX, currNode->point.y)); // midleft
            add_dead_cell(dead_cells, rootNode, vec2i(rightX, currNode->point.y)); // midright
        }
    } else {
        QuadtreeSimulateLife(rootNode, currNode->sw, dead_cells, cellSize);
        QuadtreeSimulateLife(rootNode, currNode->nw, dead_cells, cellSize);
        QuadtreeSimulateLife(rootNode, currNode->ne, dead_cells, cellSize);
        QuadtreeSimulateLife(rootNode, currNode->se, dead_cells, cellSize);
    }
}

void QuadtreeRender(Quadtree_Node* currNode, int cellSize, Array<vec2>* cellData,
                       Array<vec2>* boundaryData) {
    if(currNode->has_no_children()) {
        if(currNode->flags == Quadtree_Node::Alive || currNode->flags == Quadtree_Node::ToBeAdded) {
            currNode->flags = Quadtree_Node::Alive;
            cellData->push_back(GetCellBottomLeft(currNode->point, cellSize));
        } else if(currNode->flags == Quadtree_Node::ToBeRemoved) {
            currNode->point = AJ_DUMMYPOINT;
            currNode->flags = Quadtree_Node::Default;
        }
    } else {
        boundaryData->push_back(vec2(currNode->region.left_center()));
        boundaryData->push_back(vec2(currNode->region.right_center()));
        boundaryData->push_back(vec2(currNode->region.bottom_center()));
        boundaryData->push_back(vec2(currNode->region.top_center()));

        QuadtreeRender(currNode->sw, cellSize, cellData, boundaryData);
        QuadtreeRender(currNode->nw, cellSize, cellData, boundaryData);
        QuadtreeRender(currNode->ne, cellSize, cellData, boundaryData);
        QuadtreeRender(currNode->se, cellSize, cellData, boundaryData);
    }
}

#define VC_EXTRALEAN
#define WIN32_LEAN_AND_MEAN
#include <windows.h>

struct file_handle {
    uint32_t content_size;
    void* contents;
};

static uint32_t 
SafeTruncateUInt64(uint64_t Value)
{
    AJ_ASSERT(Value <= 0xFFFFFFFF)
    return((uint32_t)Value);
}

void
FreeFileMemory(void* Memory)
{
    if(Memory)
    {
        VirtualFree(Memory, 0, MEM_RELEASE);
    }
}

file_handle
ReadEntireFile(const char* Filename)
{
    file_handle Result = {0};
    HANDLE FileHandle = CreateFileA(Filename,
                                    GENERIC_READ,
                                    FILE_SHARE_READ,
                                    0,
                                    OPEN_EXISTING,
                                    0,
                                    0);
    if(FileHandle != INVALID_HANDLE_VALUE)
    {
        LARGE_INTEGER FileSize;
        if(GetFileSizeEx(FileHandle, &FileSize))
        {
            uint32_t FileSize32 = SafeTruncateUInt64(FileSize.QuadPart);
            Result.contents = VirtualAlloc(0, FileSize32, MEM_RESERVE|MEM_COMMIT, PAGE_READWRITE);
            if(Result.contents)
            {
                DWORD BytesRead = 0;
                if(ReadFile(FileHandle, Result.contents, FileSize32, &BytesRead, 0) && 
                  (BytesRead == FileSize32))
                {
                    Result.content_size = FileSize32;
                }
                else
                {
                    FreeFileMemory(Result.contents);
                    Result.contents = 0;
                } 
            }
            else
            {

            }
        }
        else
        {

        }
        CloseHandle(FileHandle);        
    }
    else
    {

    }
    return(Result);
}

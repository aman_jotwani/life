@echo off

if not defined DevEnvDir call "w:\src\buildshell.bat"

set INCLUDE=w:\libs\SDL2-2.0.4\include;%INCLUDE%
set INCLUDE=w:\libs\glew\include;%INCLUDE%

set LIB=w:\libs\SDL2-2.0.4\lib\x64;%LIB%
set LIB=w:\libs\glew\lib\Release\x64;%LIB%

set DebugFlags=-MTd -nologo -Gm- -GR- -EHa- -EHs -Zo -FC -Z7 -W3 -D_CRT_SECURE_NO_WARNINGS
set ReleaseFlags=-MTd -nologo -Gm- -GR- -EHa- -EHs -D_CRT_SECURE_NO_WARNINGS
set IgnoreWarnings=-wd4100 -wd4244

mkdir w:\build
pushd w:\build
cl %DebugFlags% %IgnoreWarnings% -Fe:GameOfLife.exe w:\src\Main.cpp /link opengl32.lib glew32s.lib SDL2.lib SDL2main.lib -NODEFAULTLIB:LIBCMT -SUBSYSTEM:CONSOLE
popd

#version 440 core

layout (location=0) in vec2 position;
layout (location=1) in vec2 offset;

uniform mat4 orthoProj;
uniform mat4 scalingMatrix;

void main() {
    mat4 translationMatrix = mat4(1.0, 0.0, 0.0, 0.0,
                                  0.0, 1.0, 0.0, 0.0,
                                  0.0, 0.0, 1.0, 0.0,
                                  offset.x, offset.y, 0.0, 1.0);
    gl_Position = orthoProj*translationMatrix*scalingMatrix*vec4(position, 1.0, 1.0);
}
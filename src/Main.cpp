// profile the simulation time
// try 1 or 2 different algorithms
// make the board infinite and add a simple camera
#include <stdio.h>
#include <stdint.h>

#define AJ_ASSERT(exp) if(!(exp)) {*(int*)0 = 0;}
#define AJ_ARRAYSIZE(array) sizeof(array) / sizeof((array)[0])

#include <SDL.h>
#undef main

#include "MathUtils.cpp"
#include "DataStructures.cpp"
#include "WinApi.cpp"
#include "RenderEngine.cpp"

#include "Quadtree.cpp"

int main(int argc, char** argv) {
    AJ_ASSERT(SDL_Init(SDL_INIT_EVERYTHING) == 0);

    // settings
    int screenWidth = 512;
    int screenHeight = 512;
    int majorVersion = 4;
    int minorVersion = 4;
    int cellSize = 8;
    int totalCells = (screenWidth / cellSize)*(screenHeight / cellSize);
    //

    SDL_Window* window = SDL_CreateWindow("Conway", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
                                          screenWidth, screenHeight,
                                          SDL_WINDOW_SHOWN|SDL_WINDOW_OPENGL);

    SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, majorVersion);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, minorVersion);
    SDL_GLContext context = SDL_GL_CreateContext(window);

    glewExperimental = GL_TRUE;
    AJ_ASSERT(glewInit() == GLEW_OK);

    uint32_t quadVao, quadVbo, quadEbo, instanceVbo;
    {
        glGenVertexArrays(1, &quadVao);

        glBindVertexArray(quadVao);

        glGenBuffers(1, &quadVbo);
        glBindBuffer(GL_ARRAY_BUFFER, quadVbo);
        glBufferData(GL_ARRAY_BUFFER, sizeof(float)*AJ_ARRAYSIZE(quadVerts), quadVerts,
                     GL_STATIC_DRAW);
        glEnableVertexAttribArray(0);
        glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, sizeof(vec2), NULL);

        glGenBuffers(1, &instanceVbo);
        glBindBuffer(GL_ARRAY_BUFFER, instanceVbo);
        glBufferData(GL_ARRAY_BUFFER, sizeof(vec2)*totalCells, NULL, GL_STATIC_DRAW);
        glEnableVertexAttribArray(1);
        glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(vec2), NULL);
        glVertexAttribDivisor(1, 1);

        glGenBuffers(1, &quadEbo);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, quadEbo);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(uint16_t)*AJ_ARRAYSIZE(quadIdxs), quadIdxs,
                     GL_STATIC_DRAW);

        glBindVertexArray(0);
    }

    uint32_t lineVao, lineVbo;
    glGenVertexArrays(1, &lineVao);
    glBindVertexArray(lineVao);
    glGenBuffers(1, &lineVbo);
    glBindBuffer(GL_ARRAY_BUFFER, lineVbo);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vec2)*totalCells, NULL, GL_STATIC_DRAW);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, sizeof(vec2), NULL);
    glBindVertexArray(0);

    GLuint boundaryShaders = RE_CompileShaders("../shaders/BoundaryVert.glsl", "../shaders/BoundaryFrag.glsl");
    GLuint liveCellShaders = RE_CompileShaders("../shaders/Vert.glsl", "../shaders/Frag.glsl");
    matrix orthoProjection = OrthoProjectionOffCenterLH(0.1f, 10.0f, (float)screenWidth, (float)screenHeight);
    matrix scalingMatrix = Scaling(cellSize, cellSize, 1);

    Quadtree_Node lifeTree(AABB(vec2i(0, 0), vec2i(screenWidth, screenHeight)));

    Array<vec2> boundaries;
    Array<vec2> instances;

    bool running = true;
    bool simulating = false;
    uint32_t last_time = 0;
    uint32_t current_time = 0;
    while(running) {
        SDL_Event event;
        while(SDL_PollEvent(&event) != 0) {
            if(event.type == SDL_QUIT ||
               (event.type == SDL_KEYDOWN && event.key.keysym.sym == SDLK_ESCAPE)) {
                running = false;
            } else if(event.type == SDL_MOUSEBUTTONDOWN) {
                vec2i mouseClick(event.button.x, screenHeight - event.button.y);
                mouseClick = GetCellCenter(mouseClick, cellSize);
                QuadtreeInsert(&lifeTree, mouseClick, cellSize);
            } else if(event.type == SDL_KEYDOWN && event.key.keysym.sym == SDLK_s) {
                simulating = simulating ? false:true;
            }
        }

        if(simulating) {
            current_time = SDL_GetTicks();
            if(current_time - last_time > 17) {
                Array<Dead_Cell> dead_cells;
                QuadtreeSimulateLife(lifeTree, &lifeTree, dead_cells, cellSize);

                for(int i = 0; i < dead_cells.size(); ++i) {
                    Dead_Cell dead_cell = dead_cells[i];
                    if(dead_cell.counter == 3) QuadtreeInsert(&lifeTree, dead_cell.coords, cellSize, Quadtree_Node::ToBeAdded);
                }

                last_time = current_time;
            }
        }

        QuadtreeRender(&lifeTree, cellSize, &instances, &boundaries);

        glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT);

        if(instances.size() != 0) {
            // render cells
            glBindVertexArray(quadVao);
            glUseProgram(liveCellShaders);
            RE_BindMatrix(liveCellShaders, "orthoProj", orthoProjection);
            RE_BindMatrix(liveCellShaders, "scalingMatrix", scalingMatrix);
            glBindBuffer(GL_ARRAY_BUFFER, instanceVbo);
            glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(vec2)*instances.size(),
                            instances.Data);
            glDrawElementsInstanced(GL_TRIANGLES, 6, GL_UNSIGNED_SHORT, NULL,
                                    instances.size());
            glUseProgram(0);
            glBindVertexArray(0);

            // render quadtree boundaries
            glBindVertexArray(lineVao);
            glBindBuffer(GL_ARRAY_BUFFER, lineVbo);
            glUseProgram(boundaryShaders);
            RE_BindMatrix(boundaryShaders, "orthoProj", orthoProjection);
            glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(vec2)*boundaries.size(),
                            boundaries.Data);
            glDrawArrays(GL_LINES, 0, boundaries.size()*2);
            glUseProgram(0);
            glBindVertexArray(0);
        }

        instances.clear();
        boundaries.clear();
        SDL_GL_SwapWindow(window);
    }

    return 0;
}
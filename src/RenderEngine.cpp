#define GLEW_STATIC
#include <GL/glew.h>

float quadVerts[] = {
    0.0f, 0.0f,
    1.0f, 0.0f,
    1.0f, 1.0f,
    0.0f, 1.0f
};

uint16_t quadIdxs[] = {
    0, 1, 2,
    2, 3, 0
};

void
RE_ReadShaderSource(const char* Filename, GLuint Shader) {
    file_handle shader_src = ReadEntireFile(Filename);              // provided by platform
    glShaderSource(Shader, 1, (const GLchar* const*)&shader_src.contents, NULL);
    glCompileShader(Shader);
    
    GLint success;
    glGetShaderiv(Shader, GL_COMPILE_STATUS, &success);
    if (!success)
    {
        GLchar info_log[512];
        glGetShaderInfoLog(Shader, 512, NULL, info_log);
        SDL_Log((char*)info_log);
    }

    FreeFileMemory(shader_src.contents);
}

GLuint
RE_CompileShaders(const char* VShaderFilename, const char* FShaderFilename) {
    GLuint VertShader = glCreateShader(GL_VERTEX_SHADER);
    RE_ReadShaderSource(VShaderFilename, VertShader);

    GLuint FragShader = glCreateShader(GL_FRAGMENT_SHADER);
    RE_ReadShaderSource(FShaderFilename, FragShader);

    GLuint Program = glCreateProgram();
    glAttachShader(Program, VertShader);
    glAttachShader(Program, FragShader);
    glLinkProgram(Program);

    return(Program);
}

void RE_BindMatrix(GLuint program, char* uniformName, matrix mat) {
    glUniformMatrix4fv(glGetUniformLocation(program, uniformName),
                       1, GL_FALSE, mat.column_major);
}

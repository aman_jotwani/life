#version 440 core

layout (location=0) in vec2 position;

uniform mat4 orthoProj;

void main() {
    gl_Position = orthoProj*vec4(position, 1.0, 1.0);
}